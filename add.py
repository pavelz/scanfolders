import argparse
import os

import db
import file

parser = argparse.ArgumentParser()
parser.add_argument("db")
parser.add_argument("files", nargs='+')
args = parser.parse_args()

connection = db.get_connection(args.db)

for f in args.files:
    if os.path.isfile(f):
        file.add_file(connection, f)
    if os.path.isdir(f):
        file.add_folder(connection, f)
