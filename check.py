import argparse

import db

parser = argparse.ArgumentParser()
parser.add_argument("-f", "--fix", action="store_true")
parser.add_argument("db")
args = parser.parse_args()

connection = db.get_connection(args.db)

for f in args.files:
    db.check(connection, args.fix)
