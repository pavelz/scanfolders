import argparse

import db

parser = argparse.ArgumentParser()
parser.add_argument("db")
args = parser.parse_args()

db.init_database(args.db)
