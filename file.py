import base64
import hashlib
import os

import db

FILE_READ_BUFFER_SIZE = 1024*1024


def calc_checksums(path):
    f = open(path, "rb")
    md5 = hashlib.md5()
    sha1 = hashlib.sha1()
    sha256 = hashlib.sha256()
    sha512 = hashlib.sha512()
    while 1:
        chunk = f.read(FILE_READ_BUFFER_SIZE)
        if not chunk:
            break
        md5.update(chunk)
        sha1.update(chunk)
        sha256.update(chunk)
        sha512.update(chunk)
    f.close()
    md5 = base64.b64encode(md5.digest()).decode()
    sha1 = base64.b64encode(sha1.digest()).decode()
    sha256 = base64.b64encode(sha256.digest()).decode()
    sha512 = base64.b64encode(sha512.digest()).decode()
    return md5, sha1, sha256, sha512, os.path.getsize(path)


def add_file(connection, path):
    """Add/update information about file """
    if not os.path.exists(path):
        raise ValueError("Error: File %s does not exists" % path)
    if not os.path.isfile(path):
        raise ValueError("Error: %s is supposed to be a file" % path)
    name = os.path.split(path)[1]
    abspath = os.path.abspath(path)
    size = os.path.getsize(path)
    ctime = os.path.getctime(path)
    mtime = os.path.getmtime(path)

    rowid = db.get_file_rowid(connection, abspath)
    if rowid:
        old_ct, old_mt, old_size, old_sha1, old_sha256, old_sha512 = db.get_file_info(connection, rowid)
        if old_ct == ctime and old_mt == mtime and old_size == size:
            print("Skip %s" % abspath.encode('utf-8'))
            return
    else:
        rowid = db.insert_file(connection, name, abspath, ctime, mtime)
        print("Insert %s" % abspath.encode('utf-8'))

    md5, sha1, sha256, sha512, size = calc_checksums(path)
    checksum_rowid = db.get_sha512_rowid(connection, sha512)
    if checksum_rowid:
        db.update_file(connection, rowid, ctime, mtime, checksum_rowid)
    else:
        checksum_rowid = db.insert_checksums(connection, md5, sha1, sha256, sha512, size)
        print("New %s" % sha1)
    db.update_file(connection, rowid, ctime, mtime, checksum_rowid)


def add_folder(connection, path):
    if not os.path.exists(path):
        raise ValueError("Error: File %s does not exists" % path)
    if not os.path.isdir(path):
        raise ValueError("Error: %s is supposed to be a folder" % path)
    lst = [os.path.join(path, name) for name in os.listdir(path)]
    for f in lst:
        if os.path.isdir(f):
            add_folder(connection, f)
        else:
            add_file(connection, f)


def find_dups(connection, path, verbose=False):
    if not os.path.exists(path):
        raise ValueError("Error: File %s does not exists" % path)

    abspath = os.path.abspath(path)

    if os.path.isfile(path):
        md5, sha1, sha256, sha512, size = calc_checksums(path)
        checksum_rowid = db.get_sha512_rowid(connection, sha512)
        if checksum_rowid:
            print(abspath)
            if verbose:
                files = db.get_files_by_checksum_rowid(connection, checksum_rowid)
                for f in files:
                    print("\t%s" % f)
    elif os.path.isdir(path):
        lst = [os.path.join(path, name) for name in os.listdir(path)]
        for f in lst:
            find_dups(connection, f, verbose)
