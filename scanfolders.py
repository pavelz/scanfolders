import os
import hashlib
import base64
import sqlite3
import sys
import getopt
import time
import pdb

FILE_READ_BUFFER_SIZE = 1024

g_print_trace_info = False
g_default_path_encoding = None
g_lazy_processing = False

g_processed_folders = 0
g_inserted_folders = 0
g_updated_folders = 0
g_processed_files = 0
g_inserted_files = 0
g_updated_files = 0
g_corrupted_files = 0

"""Create necessary tables
"""
def initDatabase(path):
    connection = sqlite3.connect(path)
    connection.executescript("""
CREATE TABLE CHECKSUM
(
	MD5 TEXT,
	SHA1 TEXT,
	LENGTH INTEGER, 
	PRIMARY KEY(MD5, SHA1)
);

CREATE TABLE FILE
(
	NAME TEXT NOT NULL,
	PATH TEXT NOT NULL,
	CREATION REAL NOT NULL,
	MODIFICATION REAL NOT NULL,
	PARENT_ID INTEGER, 
	CHECKSUM_ID INTEGER,
	FOREIGN KEY (PARENT_ID) REFERENCES FILE(ROWID),
	FOREIGN KEY (CHECKSUM_ID) REFERENCES CHECKSUM(ROWID)
);

CREATE UNIQUE INDEX INDEX_FILE_PATH ON FILE (PATH ASC);
CREATE INDEX INDEX_FILE_PARENT_ID ON FILE (PARENT_ID ASC);
CREATE INDEX INDEX_FILE_CHECKSUM_ID ON FILE (CHECKSUM_ID ASC);
""")
    connection.commit()
    connection.execute("INSERT INTO CHECKSUM VALUES(?, ?, 0)",
                       (base64.b64encode(hashlib.md5().digest()), 
                        base64.b64encode(hashlib.sha1().digest())))
    connection.commit()
    return connection

def calcChecksums(path):
    #pdb.set_trace()
    file = open(path, "rb")
    md5 = hashlib.md5()
    sha1 = hashlib.sha1()
    while 1:
        chunk = file.read(FILE_READ_BUFFER_SIZE)
        if not chunk:
            break
        md5.update(chunk)
        sha1.update(chunk)
    file.close()
    md5 = base64.b64encode(md5.digest()).decode()
    sha1 = base64.b64encode(sha1.digest()).decode()
    return (md5, sha1, os.path.getsize(path))

def getChecksumId(path, connection):
    (md5, sha1, size) = calcChecksums(path)
    rowChecksum = connection.execute("SELECT ROWID FROM CHECKSUM WHERE MD5=? AND SHA1=?", (md5, sha1)).fetchone()
    if rowChecksum:
        return rowChecksum[0]
    return None

def updateChecksums(path, connection):
    (md5, sha1, size) = calcChecksums(path)
    rowChecksum = connection.execute("SELECT ROWID FROM CHECKSUM WHERE MD5=? AND SHA1=?", (md5, sha1)).fetchone()
    if rowChecksum:
        return rowChecksum[0]
    else:
        cursor = connection.execute("INSERT INTO CHECKSUM VALUES(?, ?, ?)",
                                    (md5, sha1, size))
        return cursor.lastrowid

""" Add/update information about file
"""
def probeFile(path, connection):
    if not os.path.exists(path):
        raise ValueError("Error: File %s does not exists" % path)
    if not os.path.isfile(path):
        raise ValueError("Error: %s is suposed to be a file" % path)
    name = os.path.split(path)[1]
    abspath = os.path.abspath(path)
    size = os.path.getsize(path)
    ctime = os.path.getctime(path)
    mtime = os.path.getmtime(path)
    if g_print_trace_info:
        print(("[%s] %s" % (time.strftime("%X"), abspath.encode('utf-8'))))
        global g_processed_files
        g_processed_files +=1

    #PARENT_ID
    rowParent = connection.execute("SELECT ROWID FROM FILE WHERE PATH=?", (os.path.split(abspath)[0],)).fetchone()
    if rowParent:
        parentId = rowParent[0]
    else:
        parentId = None

    # INSTERT or UPDATE
    row = connection.execute("SELECT ROWID,* FROM FILE WHERE PATH=?", 
                             (abspath,)).fetchone()
    if row:
        if (row['CREATION'] != ctime) or (row['MODIFICATION'] != mtime) or (row['PARENT_ID'] != parentId):
            checksumId = updateChecksums(path, connection)
            connection.execute("""
UPDATE FILE 
SET CREATION=?, MODIFICATION=?, PARENT_ID=?, CHECKSUM_ID=? WHERE ROWID=?""",
                               (ctime, mtime, parentId, checksumId, row['ROWID']))
            if g_print_trace_info:
                global g_updated_files
                g_updated_files +=1
        else:
            if not g_lazy_processing:
                checksumId = updateChecksums(path, connection)
                if row['CHECKSUM_ID'] != checksumId:
                    global g_corrupted_files
                    g_corrupted_files +=1
    else:
        checksumId = updateChecksums(path, connection)
        connection.execute("INSERT INTO FILE VALUES(?, ?, ?, ?, ?, ?)",
                           (name, abspath, ctime, mtime, parentId, checksumId))
        if g_print_trace_info:
            global g_inserted_files
            g_inserted_files +=1
    connection.commit()

def deleteRow(rowId, connection):
    cursor = connection.execute("SELECT ROWID FROM FILE WHERE PARENT_ID=?", 
                                (rowId,))
    namesDeleted = []
    for row in cursor:
        namesDeleted.append(row[0])
    for childRowId in namesDeleted:
        deleteRow(childRowId, connection)

    connection.execute("DELETE FROM FILE WHERE ROWID=?", (rowId,))
    connection.commit()

""" Add/update information about file
"""
def probeFolder(path, connection):
    if not os.path.exists(path):
        raise ValueError("Error: File %s does not exists" % path)
    if not os.path.isdir(path):
        raise ValueError("Error: %s is suposed to be a folder" % path)
    name = os.path.split(path)[1]
    abspath = os.path.abspath(path)
    ctime = os.path.getctime(path)
    mtime = os.path.getmtime(path)
    if g_print_trace_info:
        print("[%s] %s" % (time.strftime("%X"), abspath.encode('utf-8')))
        global g_processed_folders
        g_processed_folders +=1

    #PARENT_ID
    rowParent = connection.execute("SELECT ROWID FROM FILE WHERE PATH=?", 
                                   (os.path.split(abspath)[0],)).fetchone()
    if rowParent:
        parentId = rowParent[0]
    else:
        parentId = None

    # INSTERT or UPDATE
    row = connection.execute("SELECT ROWID,* FROM FILE WHERE PATH=?", 
                             (abspath,)).fetchone()
    if row:
        rowId = row['ROWID']
        if row['CREATION'] != ctime or row['MODIFICATION'] != mtime or row['PARENT_ID'] != parentId:
            connection.execute("""
UPDATE FILE 
SET CREATION=?, MODIFICATION=?, PARENT_ID=? WHERE ROWID=?""",
                               (ctime, mtime, parentId, rowId))
            if g_print_trace_info:
                global g_updated_folders
                g_updated_folders +=1
    else:
        cursor = connection.execute("""
INSERT INTO FILE 
VALUES(?, ?, ?, ?, ?, NULL)""",
                                    (name, abspath, ctime, mtime, parentId))
        if g_print_trace_info:
            global g_inserted_folders
            g_inserted_folders +=1
        rowId = cursor.lastrowid
    connection.commit()

    #probe files and subfolders
    list = [os.path.join(path, name) for name in os.listdir(path)]
    for f in list:
        if os.path.isdir(f):
            probeFolder(f, connection)
        else:
            probeFile(f, connection)

    #delete records for files and folders that no longer exist
    cursor = connection.execute("SELECT ROWID,* FROM FILE WHERE PARENT_ID=?", 
                                (rowId,))
    names = set(os.listdir(path))
    namesDeleted = []
    for row in cursor:
        if row['NAME'] not in names:
            namesDeleted.append(row['ROWID'])
    for rowId in namesDeleted:
        deleteRow(rowId, connection)

def listFileDubs(path, connection):
    abspath = os.path.abspath(path)
    row = connection.execute("SELECT ROWID,* FROM FILE WHERE PATH=?", 
                             (abspath,)).fetchone()
    if not row:
        raise ValueError("Error: Path %s is not in the database" % path)
    if not row['CHECKSUM_ID']:
        raise ValueError("Error: Path %s is not file" % path)

    cursor = connection.execute("SELECT ROWID,* FROM FILE WHERE CHECKSUM_ID=?", 
                                (row['CHECKSUM_ID'],))
    result = []
    for row in cursor:
        if row['PATH'] != abspath:
            result.append(row['PATH'])
    return result

""" Calc checksum and list files in DB with same checksum
"""
def listFileDubs0(path, connection):
    if not os.path.exists(path):
        raise ValueError("Error: File %s does not exists" % path)
    if not os.path.isfile(path):
        raise ValueError("Error: %s is suposed to be a file" % path)
    checksumId = getChecksumId(path, connection)
    if checksumId:
        cursor = connection.execute("SELECT ROWID,* FROM FILE WHERE CHECKSUM_ID=?", 
                                    (checksumId,))
        count = 0
        for row in cursor:
            sys.stdout.write("[%07d] %s -> %s\n" % (checksumId, path, row['PATH']))
            count += 1
        if count == 0:
            sys.stderr.write("[%07d] %s\n" % (checksumId, path))
    else:
        sys.stderr.write("[%07d] %s\n" % (0, path))

""" Calc checksum and remove file
"""
def rmFileDubs0(path, connection):
    if not os.path.exists(path):
        raise ValueError("Error: File %s does not exists" % path)
    if not os.path.isfile(path):
        raise ValueError("Error: %s is suposed to be a file" % path)
    checksumId = getChecksumId(path, connection)
    if checksumId:
        cursor = connection.execute("SELECT ROWID,* FROM FILE WHERE CHECKSUM_ID=?", 
                                    (checksumId,))
        for row in cursor:
            os.remove(path)
            break

def listFolderDubs(path, connection):
    if not os.path.exists(path):
        raise ValueError("Error: File %s does not exists" % path)
    if not os.path.isdir(path):
        raise ValueError("Error: %s is suposed to be a folder" % path)

    mapFolderName2Data = {}
    srcFolderPath = os.path.abspath(path)
    for childName in os.listdir(path):
        childPath = os.path.join(srcFolderPath, childName)
        if os.path.isdir(childPath):
            fdubs = listFolderDubs(childPath, connection)
            for (pathA, pathB, AandB, AnotB, BnotA, remainder) in fdubs:
                if len(AnotB) == 0 and len(BnotA) == 0:
                    [dstFolderPath, dstChildName] = os.path.split(pathB)
                    if dstFolderPath in mapFolderName2Data:
                        (s1, d1, data) = mapFolderName2Data[dstFolderPath]
                        s1.add(childName)
                        d1.add(dstChildName)
                    else:
                        s1 = set([childName])
                        d1 = set([dstChildName])
                        data = []
                    mapFolderName2Data[dstFolderPath] = (s1, d1, data)
                else:
                    dataRec = (pathA, pathB, AandB, AnotB, BnotA, remainder)
                    dstFolderPath = os.path.split(pathB)[0]
                    if dstFolderPath in mapFolderName2Data:
                        (s1, d1, data) = mapFolderName2Data[dstFolderPath]
                        data.append(dataRec)
                    else:
                        s1 = d1 = set()
                        data = [dataRec]
                    mapFolderName2Data[dstFolderPath] = (s1, d1, data)
        else:
            fdubs = listFileDubs(childPath, connection)
            for fdub in fdubs:
                [dstFolderPath, dstChildName] = os.path.split(fdub)
                if dstFolderPath in mapFolderName2Data:
                    (s1, d1, data) = mapFolderName2Data[dstFolderPath]
                    s1.add(childName)
                    d1.add(dstChildName)
                else:
                    s1 = set([childName])
                    d1 = set([dstChildName])
                    data = []
                mapFolderName2Data[dstFolderPath] = (s1, d1, data)

    result = []
    srcFolderFileNames = set(os.listdir(path))
    for dstFolderPath, (src, dst, diff) in mapFolderName2Data.items():
        if len(src) == 0:
            if g_print_trace_info:
                printFolderDiff(diff)
        else:
            dstFolderFileNames = set(os.listdir(dstFolderPath))
            if len(diff) > 0:
                tmp = [(os.path.split(pathA)[1], os.path.split(pathB)[1])
                       for (pathA, pathB, p3, p4, p5, p6) in diff]
                diffA, diffB = zip(*tmp)
            else:
                diffA = []
                diffB = []
            diffA = set(diffA)
            diffB = set(diffB)
            srcOnlyChildren = (srcFolderFileNames - src) - diffA
            dstOnlyChildren = (dstFolderFileNames - dst) - diffB
            sameEmptyFolders = set()
            for childName in srcOnlyChildren:
                childPath = os.path.join(srcFolderPath, childName)
                if os.path.isdir(childPath) and len(os.listdir(childPath)) == 0:
                    if childName in dstOnlyChildren:
                        dstChildPath = os.path.join(dstFolderPath, childName)
                        if os.path.isdir(dstChildPath) and len(os.listdir(dstChildPath)) == 0:
                            sameEmptyFolders.add(childName)
            src |= sameEmptyFolders
            srcOnlyChildren -= sameEmptyFolders
            dstOnlyChildren -= sameEmptyFolders
            result.append((
                    srcFolderPath, 
                    dstFolderPath, 
                    src, 
                    srcOnlyChildren, 
                    dstOnlyChildren, 
                    diff))

    return result

def listFolderDubs0(path, connection):
    if not os.path.exists(path):
        raise ValueError("Error: File %s does not exists" % path)
    if not os.path.isdir(path):
        raise ValueError("Error: %s is suposed to be a folder" % path)

    srcFolderPath = os.path.abspath(path)
    for childName in os.listdir(path):
        childPath = os.path.join(srcFolderPath, childName)
        if os.path.isdir(childPath):
            listFolderDubs0(childPath, connection)
        else:
            listFileDubs0(childPath, connection)

def rmFolderDubs0(path, connection):
    if not os.path.exists(path):
        raise ValueError("Error: File %s does not exists" % path)
    if not os.path.isdir(path):
        raise ValueError("Error: %s is suposed to be a folder" % path)

    srcFolderPath = os.path.abspath(path)
    for childName in os.listdir(path):
        childPath = os.path.join(srcFolderPath, childName)
        if os.path.isdir(childPath):
            rmFolderDubs0(childPath, connection)
        else:
            rmFileDubs0(childPath, connection)

def listFilesWithoutBackups(path, connection, path2 = None):
    if not os.path.exists(path):
        raise ValueError("Error: Path %s does not exists" % path)

    if path2: path2 = os.path.abspath(path2)
    srcFolderPath = os.path.abspath(path)
    if os.path.isfile(path):
        list = [srcFolderPath]
    else:
        list = [os.path.join(srcFolderPath, name) for name in os.listdir(path)]

    result = []
    for f in list:
        if os.path.isdir(f):
            result.extend(listFilesWithoutBackups(f, connection, path2))
        else:
            found = False
            dubs = listFileDubs(f, connection)
            if len(dubs) > 0:
                if not path2:
                    found = True
                else:
                    for dub in dubs:
                        if dub.find(path2) == 0:
                            found = True
                            break
            if found:
                if g_print_trace_info:
                    print("%s -> %s" % (f.encode('utf-8'), dub.encode('utf-8')))
            else:
                result.append(f)
                if g_print_trace_info:
                    print("[*] %s" % (f.encode('utf-8')))

    return result

def printFilesWhichExistInDb(path, connection):
    if not os.path.exists(path):
        raise ValueError("Error: Path %s does not exists" % path)
    listFolderDubs0(path, connection)

def removeFilesWhichExistInDb(path, connection):
    if not os.path.exists(path):
        raise ValueError("Error: Path %s does not exists" % path)
    rmFolderDubs0(path, connection)

def printFolderDiff(fdubs):
    for (pathA, pathB, AandB, AnotB, BnotA, data) in fdubs:
        print("%s vs %s" % (pathA.encode('utf-8'), pathB.encode('utf-8')))
        print("%s, %s, %s" % (len(AandB), len(AnotB), len(BnotA)))
        if len(AandB) < 10:
            for path in AandB:
                print("\t\t%s" % path.encode('utf-8'))
        if len(AnotB) + len(BnotA) > 0:
            print("\t%s" % pathA.encode('utf-8'))
            for path in AnotB:
                print("\t\t%s" % path.encode('utf-8'))
            print("\t%s" % pathB.encode('utf-8'))
            for path in BnotA:
                print("\t\t%s" % path.encode('utf-8'))
        """for name in AandB:
            print("%s" % (name.encode('utf-8')))
        for name in BnotA:
            print("+ %s" % (name.encode('utf-8')))
        for name in AnotB:
            print("- %s" % (name.encode('utf-8')))"""
        printFolderDiff(data)

"""Print information about usage
"""
def usage():
    print("""Options:
--verbose -v - print names of scaned files
--lazy -l
--encoding, -e
--database=_path_ -d - path to database file
--path=_path_ -p - path to scan
--init -i - init database

example:
scanfolders.py -oinit -dphotos.db
C:/Python26/python D:/prj/scanfolders/scanfolders.py -e1251 -vl -dC:/Users/Public/Documents/prj/fileinfo.db -pD:/books
scanfolders.py -opf -dphotos.db -pE:/p4/photos/raw/2010/12
scanfolders.py -opf -dphotos.db -pI:/fromCF2 1>present.txt 2>absent.txt
""")

#DB_FILE_PATH = "C:/Users/Public/Documents/prj/fileinfo.db"

def printStatistics():
    print("""--------------------------------------------------
Processed folders: %i
Inserted folders: %i
Updated folders: %i

Processed files: %i
Inserted files: %i
Updated files: %i
Corrupted files:%i
""" % (g_processed_folders, g_inserted_folders, g_updated_folders, g_processed_files, g_inserted_files, g_updated_files, g_corrupted_files))


"""
"""
def main(argv):
    try:
        opts, args = getopt.getopt(argv, 
                                   "hvle:d:p:o:b:", 
                                   ["help", "verbose", "lazy", "encoding=", "database=", "path=", "operation=", "path2="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    if opts == []:
        usage()
        sys.exit()

    op = "scan"
    path2 = None
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit()
        elif opt in ("-v", "--verbose"):
            global g_print_trace_info
            g_print_trace_info = True
        elif opt in ("-l", "--lazy"):
            global g_lazy_processing
            g_lazy_processing = True
        elif opt in ("-e", "--encoding"):
            global g_default_path_encoding
            g_default_path_encoding = arg
        elif opt in ("-d", "--database"):
            dbPath = arg
            if g_default_path_encoding:
                dbPath = arg.decode(g_default_path_encoding)
            connection = sqlite3.connect(dbPath)
            connection.row_factory = sqlite3.Row
        elif opt in ("-p", "--path"):
            path = arg
            if g_default_path_encoding:
                path = arg.decode(g_default_path_encoding)
            if not os.path.exists(path):
                print("Wrong path: '%s'" % path)
                sys.exit(2)
        elif opt in ("-b", "--path2"):
            path2 = arg
            if g_default_path_encoding:
                path2 = arg.decode(g_default_path_encoding)
        elif opt in ("-o", "--operation"):
            op = arg

    if g_print_trace_info:
        startTime = time.strftime("%X")

    if op == "init":
        initDatabase(dbPath)
    elif op == "scan":
        if os.path.isfile(path):
            probeFile(path, connection)
        else:
            probeFolder(path, connection)
        if g_print_trace_info:
            print(("[%s] - [%s]" % (startTime, time.strftime("%X"))))
            printStatistics()
    elif op == "cbkp":
        list = listFilesWithoutBackups(path, connection, path2)
        for f in list:
            print(f)
        print("Total - %s" % len(list))
    elif op == "cdup":
        if g_print_trace_info:
            print("%s" % (path.encode('utf-8')))
        fdubs = listFolderDubs(path, connection)
        if g_print_trace_info:
            printFolderDiff(fdubs)
    elif op == "pf":
        printFilesWhichExistInDb(path, connection)
    elif op == "rf":
        removeFilesWhichExistInDb(path, connection)

if __name__ == "__main__":
    main(sys.argv[1:])
