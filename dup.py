import argparse

import db
import file

parser = argparse.ArgumentParser()
parser.add_argument("-v", "--verbose", action="store_true")
parser.add_argument("db")
parser.add_argument("files", nargs='+')
args = parser.parse_args()

connection = db.get_connection(args.db)

for f in args.files:
    file.find_dups(connection, f, args.verbose)
