import base64
import hashlib
import os
import sqlite3


def init_database(path):
    """Create necessary tables"""
    connection = sqlite3.connect(path)
    connection.executescript("""
CREATE TABLE CHECKSUM
(
    MD5 TEXT,
    SHA1 TEXT,
    SHA256 TEXT,
    SHA512 TEXT,
    SIZE INTEGER,
    PRIMARY KEY(SHA1)
);

CREATE UNIQUE INDEX INDEX_SHA256 ON CHECKSUM (SHA256 ASC);
CREATE UNIQUE INDEX INDEX_SHA512 ON CHECKSUM (SHA512 ASC);

CREATE TABLE FILE
(
    NAME TEXT NOT NULL,
    PATH TEXT NOT NULL,
    CREATION REAL NOT NULL,
    MODIFICATION REAL NOT NULL,
    CHECKSUM_ID INTEGER,
    FOREIGN KEY (CHECKSUM_ID) REFERENCES CHECKSUM(ROWID)
);

CREATE UNIQUE INDEX INDEX_FILE_PATH ON FILE (PATH ASC);
CREATE INDEX INDEX_FILE_CHECKSUM_ID ON FILE (CHECKSUM_ID ASC);
""")
    connection.commit()
    connection.execute("INSERT INTO CHECKSUM VALUES(?, ?, ?, ?, 0)",
                       (base64.b64encode(hashlib.md5().digest()),
                        base64.b64encode(hashlib.sha1().digest()),
                        base64.b64encode(hashlib.sha256().digest()),
                        base64.b64encode(hashlib.sha512().digest())))
    connection.commit()
    return connection


def get_connection(db_path):
    connection = sqlite3.connect(db_path)
    connection.row_factory = sqlite3.Row
    return connection


def insert_checksums(connection, md5, sha1, sha256, sha512, size):
    cursor = connection.execute("INSERT INTO CHECKSUM VALUES(?, ?, ?, ?, ?)",
                                (md5, sha1, sha256, sha512, size))
    return cursor.lastrowid


def insert_file(connection, name, abspath, ctime, mtime):
    cursor = connection.execute("INSERT INTO FILE VALUES(?, ?, ?, ?, ?)",
                                (name, abspath, ctime, mtime, 0))
    return cursor.lastrowid


def get_sha1_rowid(connection, sha1):
    row_id = connection.execute(
        "SELECT ROWID FROM CHECKSUM WHERE SHA1=?",
        (sha1,)).fetchone()
    if row_id:
        return row_id[0]
    return None


def get_sha256_rowid(connection, sha256):
    row_id = connection.execute(
        "SELECT ROWID FROM CHECKSUM WHERE SHA256=?",
        (sha256,)).fetchone()
    if row_id:
        return row_id[0]
    return None


def get_sha512_rowid(connection, sha512):
    row_id = connection.execute(
        "SELECT ROWID FROM CHECKSUM WHERE SHA512=?",
        (sha512,)).fetchone()
    if row_id:
        return row_id[0]
    return None


def get_file_rowid(connection, abspath):
    row = connection.execute("SELECT ROWID FROM FILE WHERE PATH=?",
                             (abspath,)).fetchone()
    if row:
        return row['ROWID']
    return None


def get_files_by_checksum_rowid(connection, rowid):
    rows = connection.execute("SELECT PATH FROM FILE WHERE CHECKSUM_ID=?",
                              (rowid,)).fetchall()
    result = []
    for row in rows:
        result.append(row['PATH'])
    return result


def get_file_info(connection, rowid):
    file_row = connection.execute("SELECT * FROM FILE WHERE ROWID=?",
                                  (rowid,)).fetchone()
    checksum_row = connection.execute("SELECT * FROM CHECKSUM WHERE ROWID=?",
                                      (file_row['CHECKSUM_ID'],)).fetchone()
    return file_row['CREATION'], file_row['MODIFICATION'], checksum_row['SIZE'],\
           checksum_row['SHA1'], checksum_row['SHA256'], checksum_row['SHA512']


def update_file(connection, rowid, ctime, mtime, checksum_rowid):
    connection.execute("UPDATE FILE SET CREATION=?, MODIFICATION=?, CHECKSUM_ID=? WHERE ROWID=?",
                       (ctime, mtime, checksum_rowid, rowid))
    connection.commit()


def check(connection, fix):
    rows = connection.execute("SELECT ROWID, PATH, CHECKSUM_ID FROM FILE").fetchall()
    missing = []
    for row in rows:
        if not os.path.exists(row['PATH']):
            print("Missing %s" % row['PATH'])
            missing.append(row['ROWID'])
            files = get_files_by_checksum_rowid(connection, row['CHECKSUM_ID'])
            for f in files:
                print("\t%s" % f)

    if fix:
        for rowid in missing:
            connection.execute("DELETE FROM FILE WHERE ROWID=?", (rowid,))
            connection.commit()
